import "./style";
import { Component } from "preact";
import ArticleCard from "./components/article-card";
import { getArticles } from "./api";

export default class App extends Component {
  state = {
    content: [],
    loading: true
  };

  async componentDidMount() {
    try {
      const content = await getArticles();
      this.setState({ content, loading: false });
    } catch (e) {
      console.error("could not get articles: ", e);
      this.setState({ loading: false });
    }
  }

  renderArticles = () => {
    const { content } = this.state;

    return (
      <section className="wrap">
        {content.map(({ title, articles }) => (
          <div>
            <h3 className="section-title">{title}</h3>
            <div className="article-row">
              {articles.map(article => <ArticleCard {...article} />)}
            </div>
          </div>
        ))}
      </section>
    );
  };

  renderError = () => <h1>Could not load articles :(</h1>;

  render() {
    const { loading, content } = this.state;

    return (
      <div>
        {loading && <h1>Loading</h1>}
        {!loading && this.renderArticles()}
        {!loading && content.length === 0 && this.renderError()}
      </div>
    );
  }
}
