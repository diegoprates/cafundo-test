const API_URL = `http://localhost:8080/`;

const groupBy = (list, keyGetter) => {
  const map = new Map();
  list.forEach(item => {
    const key = keyGetter(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [item]);
    } else {
      collection.push(item);
    }
  });
  return map;
};

export const getArticles = () =>
  fetch(`${API_URL}articles/`)
    .then(resp => resp.json()) // Transform the data into json
    .then(data => {
      const groupArticlesByTopic = groupBy(
        data,
        article => article.topic.title
      );
      let content = [];

      groupArticlesByTopic.forEach((value, key) => {
        content = [
          ...content,
          {
            title: key,
            articles: value
          }
        ];
      });

      return content;
    });
