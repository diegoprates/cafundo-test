import { Component } from "preact";

export default class ArticleCard extends Component {
  render() {
    const { image, url, title, topic, author, published_at } = this.props;
    return (
      <article className="article-item">
        <img src={image} />
        <p>
          <span>{topic.label}</span>
          <span className="article-spacer">|</span>
          <a href={topic.url}>{topic.title}</a>
        </p>
        <a href={url} title={title} className="article-title">
          {title}
        </a>
        <p>
          <a href={author.url}>{author.name}</a>
          <span className="article-spacer">|</span>
          <span>{published_at}</span>
        </p>
      </article>
    );
  }
}
