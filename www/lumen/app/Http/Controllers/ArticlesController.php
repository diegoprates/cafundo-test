<?php

namespace App\Http\Controllers;

use Goutte\Client;
use App\User;

class ArticlesController extends Controller
{    
    /**
     * Retrieve all articles for the given category.
     *
     * @param  string  $category
     * @return Response
     */
    public function getAll($category = "life-evolution")
    {
        $url = "https://www.sciencenews.org";
        $client = new Client();
        $app = app();
        $crawler = $client->request("GET", $url."/topic/".$category);        

        $articles = $crawler->filter('.subtopic-listing-3-items .views-field-view .views-row')->each(function ($node) use ($app, $url) { 
            $response = $app->make('stdClass');            
            $response->title = $node->filter('.views-field-title a')->text();    
            $response->image = $node->filter('.views-field-field-op-main-image a img')->last()->attr('src');        
            $response->url = "{$url}{$node->filter('.views-field-title a')->attr('href')}";        
            
            $topic = $app->make('stdClass');
            $topic_full = explode("|", $node->filter('.views-field-nothing')->text());
            $topic->label = trim($topic_full[0]);
            $topic->title = trim($topic_full[1]);
            $topic->url = "{$url}{$node->filter('.views-field-nothing a')->attr('href')}";
            
            $author = $app->make('stdClass');            
            $author_full = explode("|", $node->filter('.views-field-published-at')->text());
            $author->name = trim($author_full[0]);
            $author->url = "{$url}{$node->filter('.views-field-published-at a')->attr('href')}";
            
            $response->published_at = trim($author_full[1]);
            $response->topic = $topic;
            $response->author = $author;
            
            return $response;
        });                
    
        return response()->json($articles, 200);        
    }
}
