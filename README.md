# Client

To start client, first install dependencies `cd client && npm i` than you can run `npm run start`to start in development or `npm run serve` to build and serve this client.

You should be able to see client at `http://localhost:8081`

# API

## Install

To start API run command below from root folder

`docker-compose up -d`

You should also install dependencies from `/www/lumen` with composer, PHP container comes with composer installed and you can use it with the command below.

`docker exec -ti cafundo-test_php_1 /bin/bash`

In this case `cafundo-test_php_1` is the name docker gave to the PHP container.

Inside this container you can run `composer install`.

You should be able to see API at `http://localhost:8080`

## Endpoints

## **/articles/:category**

Returns json with all articles.

- **Method:**

  `GET`

- **URL Params**

  **Optional:**

  `category=[string]`

- **Data Params**

  None

- **Success Response:**

  - **Code:** 200 <br />
    **Content**

```json
[
    {
        title: "Got an environmental problem? Beavers could be the solution",
        image: "https://www.sciencenews.org/sites/default/files/styles/large/public/2018/07/main/articles/080418_reviews_feat.jpg?itok=F2st1Xl_",
        url: "https://www.sciencenews.org/article/got-environmental-problem-beavers-could-be-solution",
        published_at: "10:49am, July 27, 2018",
        topic: {
            label: "Reviews & Previews",
            title: "Animals",
            url: "https://www.sciencenews.org/search?tt=77"
        },
        author: {
            name: "By Sarah Zielinski",
            url: "https://www.sciencenews.org/author/sarah-zielinski"
        }
    }
```
